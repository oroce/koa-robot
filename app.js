require('dotenv-safe').load();
const config = {
  bitrisePAT: process.env.BITRISE_PAT,
  bitriseApiTokens: JSON.parse((process.env.BITRISE_API_TOKENS || '').replace(/\\"/gm, '"')),
  hipchatToken: process.env.HIPCHAT_TOKEN,
  port: process.env.PORT || 8081
};
const Koa = require('koa');
const request = require('request-promise-native');
const app = new Koa();
const koaBody = require('koa-body');
app.use(koaBody());
app.use(async (ctx) => {
  const body = ctx.request.body;
  if (body.event !== 'room_message') {
    ctx.status = 404;
    ctx.body = {
      message: `unknown event: ${body.event}`
    };
    return;
  }
  const msg = body.item.message.message;
  const match = msg.match(/^\/build (.*)@(major|minor|patch)/i);
  if (!match) {
    ctx.status = 404;
    ctx.body = {
      message: `unknown message: ${msg}`
    };
    return;
  }

  const repo = match[1];
  const version = match[2];
  const room = body.item.room.id;
  const apps = await request({
    url: 'https://api.bitrise.io/v0.1/me/apps',
    headers: {
      'Authorization': config.bitrisePAT
    },
    json: true,
  });

  const app = apps.data.find(app => app.title === repo);
  console.log(app);
  if (!app) {
    ctx.status = 404;
    ctx.body = {
      message: `Repository (${repo}) was not found.`
    };
    const list = apps.data.map(app => app.title);
    await reply(room, {
      message: `Repository ${repo} was not found.\nBut feel free to pick from this list: ${list.join(', ')}`,
      color: 'red'
    });
    return;
  }
  const appSlug = app.slug;
  const apiToken = config.bitriseApiTokens[app.title];
  const resp = await request({
    url: `https://www.bitrise.io/app/${appSlug}/build/start.json`,
    method: 'POST',
    json: true,
    body: {
      hook_info: {
        type: 'bitrise',
        api_token: apiToken
      },
      build_params: {
        branch: 'master',
        environments: [{
          mapped_to: 'RELEASE_TYPE',
          value: version,
          is_expand: true
        }]
      }
    }
  });
  await reply(room, {
    message: `Build has been started for ${repo} with version ${version}: ${resp.build_url}`,
  });
  ctx.body = 'Hello World';
});

function reply(room, options = {}) {
  return request({
    url: `https://creapps.hipchat.com/v2/room/${room}/notification`,
    qs: {
      'auth_token': config.hipchatToken
    },
    method: 'POST',
    json: true,
    form: Object.assign({
      color: 'green',
      notify: false,
      message_format: 'text'
    }, options)

  });
}

app.listen(config.port);
